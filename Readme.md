to build

```bash
mkdir build
cd build
cmake ../
make
```

to run

```bash
cd examples
../build/bin/molecule.X
```
fortuno for testing

```bash
git clone https://github.com/aradi/fortuno.git
git clone https://gitlab.com/drFaustroll/molecule.git
cd molecule
FC=ifort cmake -S ~/fortuno -B build_fortuno -DCMAKE_INSTALL_PREFIX=$(pwd)/fortuno
cmake --build build_fortuno
cmake --install build_fortuno
CMAKE_PREFIX_PATH=/home/vol02/scarf562/molecule/fortuno/  FC=ifort cmake -S../ -B build_molecule
cmake --build build_molecule
cd examples
../build_molecule/bin/test.x
```
